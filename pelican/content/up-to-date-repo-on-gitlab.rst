Up-to-date Repo on GitLab
#########################
:date: 2018-07-16 16:02
:author: asciidoc3
:category: News
:slug: up-to-date-repo-on-gitlab
:status: published

The former repo on GitHub github.com/asciidoc3/asciidoc3 is still there.
But from now on it serves as a 'starting point' to the up-to-date repo
on GitLab https://gitlab.com/asciidoc3/asciidoc3. Please use the new
location for all your comments/issues/merge-requests.
