v3.0.1 on GitHub
################
:date: 2018-05-18 13:51
:author: asciidoc3
:category: News
:slug: v3-0-1-on-github
:status: published

| The source on https://github.com/asciidoc3/asciidoc3 is now tagged as:
| v3.0.1 ... have fun with AsciiDoc3 using Python3!
