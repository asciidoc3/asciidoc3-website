Contact Form Switched Off
#########################
:date: 2018-06-22 11:45
:author: asciidoc3
:category: News
:slug: contact-form-switched-off
:status: published

| Due to an increasing amount of spam we decided to switch off the
  contact form. Please use your favorite mail-client:
| info ~at~ asciidoc3.org.
