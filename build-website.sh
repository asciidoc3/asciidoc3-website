#!/bin/sh

VERS="3.2.3"
DATE="2021-12-09"

# Leave the desired layout uncommented.
#LAYOUT=layout1              # Tables based layout.
#LAYOUT=layout2              # CSS based layout.
#LAYOUT=responsivelayout     # CSS responsive.
LAYOUT=responsive_menu       # CSS responsive menu.

ASCIIDOC3_HTML="asciidoc3 --backend=xhtml11 --conf-file=${LAYOUT}.conf --attribute icons --attribute iconsdir=../../images/icons --attribute=badges --attribute=revision=$VERS  --attribute=date=$DATE"

echo -n .
$ASCIIDOC3_HTML -a index-only index.txt
echo -n  .
#$ASCIIDOC3_HTML -a toc -a toclevels=3 -a numbered userguide.txt
$ASCIIDOC3_HTML -a toc -a numbered userguide.txt
echo -n   .
$ASCIIDOC3_HTML -a toc -a numbered download.txt
$ASCIIDOC3_HTML -a toc -a numbered install.txt
$ASCIIDOC3_HTML -a toc -a numbered contact.txt
echo -n   .
$ASCIIDOC3_HTML -a toc -a numbered quickstart.txt
$ASCIIDOC3_HTML -a toc -a numbered pypi.txt
$ASCIIDOC3_HTML -a toc -a numbered docker.txt
$ASCIIDOC3_HTML cheatsheet.txt
$ASCIIDOC3_HTML -a toc -a numbered releasenotes.txt
echo -n      .
#$ASCIIDOC3_HTML -a toc -a numbered donate.txt
$ASCIIDOC3_HTML -a toc -a numbered legal.txt
echo -n       .
#$ASCIIDOC3_HTML -a toc -a numbered asciidoc3port.txt
$ASCIIDOC3_HTML -a toc -a toclevels=3 -a numbered windows.txt
echo          done.

#$ASCIIDOC3_HTML -d manpage manpage.txt

