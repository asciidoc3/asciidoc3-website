Site Update and GitLab
######################
:date: 2018-06-17 13:50
:author: asciidoc3
:category: News
:slug: site-update-and-gitlab
:status: published

| The site asciidoc3.org was updated: broken links, minor text
  rearrangements et al.
| You can find the "tests" from now on at
  https://gitlab.com/asciidoc3/asciidoc3-tests
| (GitLab, not GitHub!) - still work in process.
| The asciidoc3-repo stays as before:
  https://github.com/asciidoc3/asciidoc3
