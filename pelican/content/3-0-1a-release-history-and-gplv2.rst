3.0.1a Release: History and GPLv2+
##################################
:date: 2018-06-11 21:20
:author: asciidoc3
:category: News
:slug: 3-0-1a-release-history-and-gplv2
:status: published

| One significant step towards AsciiDoc3-3.0.2 is made: The 3.0.1a
  release (github.com/asciidoc3/asciidoc3) now contains the complete
  history of the project. In addition the pending 'copyright issue' is
  solved: Free use of AsciiDoc3 is well and truly granted under the
  terms of the GNU General Public License version 2 or later (GPLv2+).
| AsciiDoc3-3.0.2 will be released within the very next days: a regex
  error under Python3.6 is to be fixed. This is already done, but some
  more tests seem helpful.
