Homepage Revision
#################
:date: 2018-08-16 18:45
:author: asciidoc3
:category: News
:slug: homepage-revision
:status: published

Our homepage https://asciidoc3.org was somewhat updated. No big deal,
only proofreading and typos.
