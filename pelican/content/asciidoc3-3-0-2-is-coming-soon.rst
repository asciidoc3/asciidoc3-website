AsciiDoc3-3.0.2 is coming soon
##############################
:date: 2018-06-09 22:21
:author: asciidoc3
:category: News
:slug: asciidoc3-3-0-2-is-coming-soon
:status: published

| ... and what's in the new release?
| - Some license issues will be adjusted,
| - some minor bugs will be fixed,
| - some documentation will be added,
| - ... (cont.).
| More information asap.
