#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'asciidoc3'
SITENAME = 'asciidoc3.org'
SITEURL = 'https://asciidoc3.org/blog'
TIMEZONE = "Europe/Paris"

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'
#DEFAULT_LANG = 'de'

PLUGIN_PATHS = ['./pelican-plugins']
PLUGINS = ['asciidoc_reader', 'asciidoc3_reader']

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('AsciiDoc3 Home', 'https://asciidoc3.org/'),
         ('AsciiDoc3 Repo', 'https://gitlab.com/asciidoc3/asciidoc3/'),
         ('AsciiDoc3 PyPI', 'https://pypi.org/project/asciidoc3/'))

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
