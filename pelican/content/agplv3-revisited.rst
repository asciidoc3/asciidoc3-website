AGPLv3 revisited
################
:date: 2018-05-10 19:45
:author: asciidoc3
:category: News
:slug: agplv3-revisited
:status: published

| ... started a question at
  `Stackexchange <https://opensource.stackexchange.com/questions/6816/gplv2-or-later-upgrade-to-agplv3>`__
  and asked a ‚well-established‘ lawyer off the record. Both answers
  confirmed my resolution to choose the AGPLv3.
| The trick is in my words: there was never a software under GPLv3 until
  now, AsciiDoc is GPLv2 or later. There is no ‚switching from GPLv3 to
  AGPLv3‘, because there is/was no GPLv3! So, according to the
  ‚\ `compatibility
  matrix <https://www.gnu.org/licenses/gpl-faq.htm>`__\ ‘, you may
  upgrade from GPLv2 to AGPLv3. All right.
