Asciidoc3 starts very soon!
###########################
:date: 2018-04-13 19:45
:author: asciidoc3
:category: News
:slug: asciidoc3-starts
:status: published

| Hello everybody out there,
| I am just preparing to release the first AsciiDoc3 release candidate.
  This will take a few days: writing the missing texts to be published
  on the website, edit github/asciidoc3 and other things. There are more
  things to do than expected - and they costs more time as expected ...
