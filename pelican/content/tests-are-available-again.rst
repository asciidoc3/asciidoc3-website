Tests are available again ...
#############################
:date: 2018-08-11 21:24
:author: asciidoc3
:category: News
:slug: tests-are-available-again
:status: published

| Take a look at https://gitlab.com/asciidoc3/asciidoc3
| You'll see there new files in directory 'tests': 'standard' tests
  suitable for AsciiDoc3 are available again. Nothing spectacular at
  this moment, but it is a good start and there are more in the
  pipeline.
