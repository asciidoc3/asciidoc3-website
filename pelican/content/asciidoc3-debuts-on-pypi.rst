AsciiDoc3 debuts on PyPI
########################
:date: 2018-10-19 20:08
:author: asciidoc3
:category: News
:slug: asciidoc3-debuts-on-pypi
:status: published

| It was a huge effort but now you can test the result by yourself:
  AsciiDoc3 is available on PyPI to be installed via pip.
| To rule out any imponderables it is tagged as 'beta', but it passes
  all tests ...
| See `here <https://asciidoc3.org/pypi.html>`__ for more detailed
  information.
