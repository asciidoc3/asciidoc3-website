New version released: AsciiDoc3-3.0.1
#####################################
:date: 2018-05-16 13:07
:author: asciidoc3
:category: News
:slug: new-version-released-asciidoc3-3-0-1
:status: published

At last: today (2018-5-16) the new version 'AsciiDoc3-3.0.1' will be
released. Compared with the release-candidate there are many
improvements: An installer let you to start 'asciidoc3' or 'a2x3' from
the command line (an uninstaller is provided, too ...). Typos are
corrected, documentation completed and so on ...
