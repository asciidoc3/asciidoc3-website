AsciiDoc3 moves to GitLab
#########################
:date: 2018-07-09 13:03
:author: asciidoc3
:category: News
:slug: asciidoc3-moves-to-gitlab
:status: published

In consequence of the MicroSoft/GitHub deal AsciiDoc3 migrates to GitLab
(see: https://about.gitlab.com/2018/06/03/movingtogitlab/) You can find
us here:

https://gitlab.com/asciidoc3/asciidoc3

The repo on GitHub is still open for comments, but all further
development will be on GitLab.
