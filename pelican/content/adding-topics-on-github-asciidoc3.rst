Adding topics on github/asciidoc3
#################################
:date: 2018-05-06 10:14
:author: asciidoc3
:category: News
:slug: adding-topics-on-github-asciidoc3
:status: published

| I added the following "topics" to
  https://github.com/asciidoc3/asciidoc3:
| - asciidoc3
| - python3
| - asciidoc
| - python3-port
| I do hope, this will help spreading AsciiDoc3 all over the world ...
