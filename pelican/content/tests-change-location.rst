Tests Change Location
#####################
:date: 2018-07-08 18:06
:author: asciidoc3
:category: News
:slug: tests-change-location
:status: published

updated 2018-9-7:

see `AsciiDoc3 moves to
GitLab <https://asciidoc3.org/blog/2018/07/09/asciidoc3-moves-to-gitlab/>`__

... But if you are interested in the existing and forthcoming
(unit-)tests, take a look at GitLab (yes, not GitHub):
gitlab.com/asciidoc3/asciidoc3

This repo contains AsciiDoc3 plus the tests in branch 'test'. So the
former test-repo became redundant and was deleted.
