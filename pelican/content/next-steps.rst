Next Steps
##########
:date: 2018-05-03 15:21
:author: asciidoc3
:category: News
:slug: next-steps
:status: published

| Hi all,
| - the installer for POSIX-systems is ready to start - I was not
  satisfied with the aap/makefile procedure, so for the present the
  script will be implemented in Python. Perhaps later we'll use 'make'
  again.
| - The same holds for the uninstall-script.
| Other upcoming changes:
| - Typos in userguide.txt and conf-files (nothing dangerous ...).
| - Adding a documentation of porting to Python3.
| - Adding some testcases.
| - etc.
| - (not definitely decided about changing license from AGPLv3 to
  GPLv3).
| - Eliminate the issues reported by 'validator.w3.org' (nothing
  dangerous, 3 warnings).
| The very next update isn't a release candidate any more; it will come
  very soon ...
