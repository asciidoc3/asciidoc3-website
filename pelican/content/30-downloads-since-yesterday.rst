30 downloads since yesterday
############################
:date: 2018-05-17 15:19
:author: asciidoc3
:category: News
:slug: 30-downloads-since-yesterday
:status: published

The first 'real' release with installer/uninstaller, (almost) complete
manual and many other enhancements went public 24h ago. Since just now,
the server counts 24 downloads of the tarball - neat! Thank you!
