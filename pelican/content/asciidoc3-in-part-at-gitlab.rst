AsciiDoc3 in Part on GitLab
###########################
:date: 2018-07-03 16:47
:author: asciidoc3
:category: News
:slug: asciidoc3-in-part-at-gitlab
:status: published

| From now on you can find two sections of the AsciiDoc3 project on
  GitLab:
| The sources of asciidoc3.org:
  https://gitlab.com/asciidoc3/asciidoc3-website and some tests (the
  'normal' user doesn't need this stuff ...)
  https://gitlab.com/asciidoc3/asciidoc3-tests

The 'core' of AsciiDoc3 remains on GitHub:
https://github.com/asciidoc3/asciidoc3
