asciidoc3.org is now responsive (... nearly)
############################################
:date: 2018-07-06 14:34
:author: asciidoc3
:category: News
:slug: asciidoc3-org-is-now-responsive-nearly
:status: published

| The hitherto existing layout was subtle revised: the main menu is now
  found underneath the banner. In addition the 'new' layout fulfilled
  some terms of 'responsive design' - not perfect yet, of course. We do
  hope that you like it!
| Sources are available: https://gitlab.com/asciidoc3/asciidoc3-website
