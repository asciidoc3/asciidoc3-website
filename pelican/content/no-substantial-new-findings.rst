No Substantial New Findings
###########################
:date: 2018-09-09 14:18
:author: asciidoc3
:category: News
:slug: no-substantial-new-findings
:status: published

The renegotiation about the license of AsciiDoc3 (started by @ArneBab)
brings no substantial new findings to me ... read more:
https://github.com/asciidoc3/asciidoc3/issues/3#issuecomment-419718677
