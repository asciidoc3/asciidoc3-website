GPLv2 or later upgrade to AGPLv3
################################
:date: 2018-05-05 10:18
:author: asciidoc3
:category: News
:slug: gplv2-or-later-upgrade-to-agplv3
:status: published

| Started a question at
  `Stackexchange <https://opensource.stackexchange.com/questions/6816/gplv2-or-later-upgrade-to-agplv3>`__:
| I modify open source software that is under ‚GPLv2 or later‘. I will
  upgrade to GPLv3 or later – that‘s of cause no problem at all. Not
  looking for advice in this case, but is it possible to upgrade to
  AGPLv3, too? The info provided at gnu.org/licenses/ are not clear for
  me. In short: GPLv2 or later -> AGPLv3, possible, yes or no?
