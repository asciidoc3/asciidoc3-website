Release Candidate AsciiDoc3 is out now!
#######################################
:date: 2018-04-17 16:34
:author: asciidoc3
:category: News
:slug: release-candidate-asciidoc3-is-out-now
:status: published

| AsciiDoc3 starts! Yesterday I deactivated the login block for `this
  website <https://www.asciidoc3.org>`__ and added the first bundle of
  files at
  `github.com/asciidoc3/asciidoc3 <https://github.com/asciidoc3/asciidoc3>`__.
| A first mail came in only a few minutes later saying "well done" -
  Thx! But there is work to do ...
