2 weeks - 60 AsciiDoc3
######################
:date: 2018-06-02 21:24
:author: asciidoc3
:category: News
:slug: 2-weeks-60-asciidoc3
:status: published

| Two weeks have gone since the going online of the latest release
  **AsciiDoc3-3.0.1**.
| We count about 60 downloads of the tarball here on asciidoc3.org - no
  information is on hand about
  `github.com/asciidoc3/asciidoc3 <https://github.com/asciidoc3/asciidoc3>`__.
  I think, this is acceptable. Thx!
