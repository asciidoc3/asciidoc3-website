Adding github tag
#################
:date: 2018-05-07 11:50
:author: asciidoc3
:category: News
:slug: adding-github-tag
:status: published

| Added a tag "3.0.1-release candidate" at
  `github-repo <https://github.com/asciidoc3/asciidoc3/>`__.
| That's only effective for a few days, because the "3.0.1 release" is
  in the starting blocks ...
