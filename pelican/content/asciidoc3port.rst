asciidoc3port
#############
:date: 2018-07-10 21:11
:author: asciidoc3
:category: News
:slug: asciidoc3port
:status: published

There is a new page on asciidoc3.org:
https://asciidoc3.org/asciidoc3port.html

There you can find information about the process of porting AsciiDoc to
AsciiDoc3 (Python3). Not mandatory when using AsciiDoc3 of course, but -
as I think - interesting for those, who want understand more about the
details of the program.
