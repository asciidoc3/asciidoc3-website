New Release: AsciiDoc3-3.0.2
############################
:date: 2018-06-15 22:24
:author: asciidoc3
:category: News
:slug: new-release-asciidoc3-3-0-2
:status: published

AsciiDoc3-3.0.2 is ready to download. Some highlights: all GPLv2+,
enhanced documentation, bug fixing, eliminating typos ...
