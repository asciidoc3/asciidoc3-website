README Update
#############
:date: 2018-08-25 20:11
:author: asciidoc3
:category: News
:slug: readme-update
:status: published

The README.md file on https://gitlab.com/assciidoc3/asciidoc3 was
updated.

The same holds for https://gitlab.com/assciidoc3/asciidoc3-website.
There you can find the sources of https://asciidoc3.org, including
'build-website.sh', which uses AsciiDoc3 to produce all the HTML-files.
