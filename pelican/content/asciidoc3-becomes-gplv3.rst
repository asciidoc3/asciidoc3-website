AsciiDoc3 becomes GPLv3
#######################
:date: 2018-05-11 09:03
:author: asciidoc3
:category: News
:slug: asciidoc3-becomes-gplv3
:status: published

I encounter some kind of ‚shitstorm‘ since my statement about choosing
the AGPLv3. All right, I do not insist on AGPL, the forthcoming release
of AsciiDoc3 will be ‚GPLv3 or later‘ ...
