Minor Commits Regex
###################
:date: 2018-07-04 22:23
:author: asciidoc3
:category: News
:slug: minor-commits-regex
:status: published

| To avoid deprecation warnings (when Python3.7 is used) some \*.conf
  and \*.py files have been edited.
| In module re '(?u)' is the default, so this is removed; (?su) becomes
  (?s), and (?msu) is now (?ms).
| The new files can be found here:
  https://github.com/asciidoc3/asciidoc3
