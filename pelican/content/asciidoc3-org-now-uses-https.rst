asciidoc3.org  now uses https
#############################
:date: 2018-04-25 09:10
:author: asciidoc3
:category: News
:slug: asciidoc3-org-now-uses-https
:status: published

| Hi all,
| a simple step to make asciidoc3.org more secure and better to find by
  'Tante Gugel' ('aunt gugel' as we say occasionally in German): I have
  added an `Let's Encrypt <https://letsencrypt.org/>`__ certificate to
  make use of the 'https'-protocol.
