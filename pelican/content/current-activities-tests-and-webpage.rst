Current Activities: Tests and Webpage
#####################################
:date: 2018-07-30 21:46
:author: asciidoc3
:category: News
:slug: current-activities-tests-and-webpage
:status: published

| What is going on about AsciiDoc3? First we are working on an advanced
  test-suite: so you can "in a flash" screen if your new version of
  asciidoc3.py produces the binary identical output compared with the
  previous version. Release date is scheduled the first week of Aug '18
  (gitlab.com/asciidoc3/asciidoc3).
| Second is the careful modification of the website
  https://asciidoc3.org towards a responsive layout. Some steps are
  already made, we have no fixed timetable here.
